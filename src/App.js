import React from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import HomePage from './containers/home/consumer'
import About from './containers/about/about'
import Team from './containers/team/consumer'
import Search from './containers/search'
import Validation from './containers/validation'
export default  () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path={"/"} exact={true} component={HomePage}/>
                <Route path={"/about"} component={About}/>
                <Route path={"/team"} component={Team}/>
                <Route path={"/search"} component={Search}/>
                <Route path={"/validation"} component={Validation}/>
            </Switch>
        </BrowserRouter>
    );
}