import React, {useState, useEffect} from 'react';
import Layout from '../../Components/layout/layout';
import arraySort from 'array-sort';
import CommonComponent from '../../Components/commonComponent/commonComponent';
export default (props) => {
    const [frontEndTeam, setFrontEndTeam] = useState([
        {id: 1, name: "Akash"},
        {id: 2, name: "Shoaib"},
        {id: 3, name: "Hamza"},
        {id: 5, name: "Zubair"},
        {id: 6, name: "Hajira"},
        {id: 7, name: "Rida"},
    ]);
    const [frontEndTeamBackup, setFrontEndTeamBackup] = useState([]);
    const searchHandler = (value) => {
        if (frontEndTeamBackup.length === 0) {
            setFrontEndTeamBackup([...frontEndTeam]);
        }
        setFrontEndTeam([...frontEndTeamBackup.filter(sin => sin.name.toLowerCase().indexOf(value.toLowerCase()) !== -1)])
    };
    return (
        <Layout>
            <input type="text" onChange={event => {
                searchHandler(event.target.value)
            }}/>
            <ul>
                {frontEndTeam.map(sin => <li>{sin.name}</li>)}
            </ul>
        </Layout>
    );
}