import React, {Component} from "react";
import {Consumer} from "../../store";
import Child from "./home";
    export default ()=> {
        return (
            <Consumer>
                {
                    ({dispatch, word, name}) => (
                        <Child dispatch={dispatch} word={word} name={name}/>
                    )
                }
            </Consumer>
        )
    }