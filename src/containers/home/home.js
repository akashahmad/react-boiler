import React, {useState, useEffect} from 'react';
import Layout from '../../Components/layout/consumer';
import SectionOne from '../../Components/sectionOne/sectionOne';
import CommonComponent from '../../Components/commonComponent/commonComponent';
import axios from 'axios';
export default (props) => {
    const [petOwner, setPetOwner] = useState([]);
    let {dispatch, word, name} = props;
    useEffect(() => {
        axios.get('https://us-central1-fur-baby-tracker-dev.cloudfunctions.net/getPetOwnerData?petOwnerUId=6SxzQI3aRTVjYZQWKUftrD0MPWi2').then(res => {
            console.log("response", res.data);
            setPetOwner(res.data.data.petOwnderDetail)
        });
    }, []);
    const setGlobalName = (value) => {
        dispatch({
            type: "NAME",
            payLoad: value
        })
    }
    return (
        <Layout>
            <button onClick={() => {
                dispatch({
                    type: "SET_WORD",
                    payLoad: Math.floor(Math.random() * 100)
                });
            }}>change text
            </button>
            <SectionOne petOwner={petOwner} global={word}/>
            <CommonComponent />
            <h1>Name</h1>
            <input type="text" value={name} onChange={(event) =>setGlobalName(event.target.value)}/>
        </Layout>
    );
}