import React, {Component} from "react";
import {Consumer} from "../../store";
import Child from "./";
    export default ()=> {
        return (
            <Consumer>
                {
                    ({dispatch, name}) => (
                        <Child dispatch={dispatch} name={name}/>
                    )
                }
            </Consumer>
        )
    }