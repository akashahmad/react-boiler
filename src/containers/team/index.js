import React from 'react';
import Layout from '../../Components/layout/consumer';
export default (props) => {
    let {name} = props;
    return (
        <Layout>
            <h1>{name}</h1>
        </Layout>
    );
}