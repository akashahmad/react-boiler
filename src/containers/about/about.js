import React, {useState, useEffect} from 'react';
import Layout from '../../Components/layout/layout';
import arraySort from 'array-sort';
import CommonComponent from '../../Components/commonComponent/commonComponent';
export default (props) => {
    const [frontEndTeam, setFrontEndTeam] = useState([
        {id: 1, name: "Akash"},
        {id: 2, name: "Shoaib"},
        {id: 3, name: "Hamza"},
        {id: 5, name: "Zubair"},
        {id: 6, name: "Hajira"},
        {id: 7, name: "Rida"},
    ]);
    const [name, setName] = useState("");
    const [id, setId] = useState("");
    const [nameSortType, setNameSortType] = useState("");

    const sortByNames = () => {
        if (!nameSortType || nameSortType === "DESC") {
            setFrontEndTeam(arraySort(frontEndTeam, 'name'));
            setNameSortType("ASC")
        } else {
            setFrontEndTeam(arraySort(frontEndTeam, 'name', {reverse: true}));
            setNameSortType("DESC")
        }
    };
    const deleteName = (id) => {
        setFrontEndTeam([...frontEndTeam.filter((sin) => sin.id !== id)])
    };
    const addMember = () => {
        let newFrontEndList = [...frontEndTeam];
        newFrontEndList.push({id: frontEndTeam.length+2, name: name});
        setFrontEndTeam(newFrontEndList);
        setName("");
    };

    const updateName = () => {
        let newFrontEndList = [...frontEndTeam];
        newFrontEndList[id].name = name;
        setFrontEndTeam(newFrontEndList);
        setName("");
        setId("");
    };

    const editName = (id) => {
        setName(frontEndTeam.find((sin, index)=>index === id).name);
        setId(id);
    };
    return (
        <Layout>
            <button
                onClick={() => sortByNames()}>{nameSortType && nameSortType === "ASC" ? "ASCENDING" : "DESCENDING"}</button>
            <ul>
                {frontEndTeam.map((single, index) => <li>{single.name} &nbsp;
                    <button onClick={() => deleteName(single.id)}>delete</button>&nbsp;
                    <button onClick={() => editName(index)}>Edit</button>
                </li>)}
            </ul>
            <input onChange={(event) => setName(event.target.value)} value={name ? name : ""} type="text"/>
            <button onClick={() => addMember()}>Add New Team Member</button>
            <button onClick={() => updateName()}>Update</button>
            <CommonComponent />
        </Layout>
    );
}