import React, {useState, useEffect} from 'react';
import Layout from '../../Components/layout/layout';
import {validateEmail} from '../../Components/utils';
export default () => {

    const [name, setName] = useState("");
    const [nameValiation, setNameValidation] = useState(false);
    const [email, setEmail] = useState("");
    const [emailValiation, setEmailValidation] = useState(false);
    const submitHandler = (event) => {
        event.preventDefault();

    };
    const customValidation = () => {
        if (!name) {
            setNameValidation(true);
        }
        if (!email || !validateEmail(email)) {
            setEmailValidation(true);
        }
    };
    return (
        <Layout>
            <form onSubmit={event => submitHandler(event)}>
                <label >Name</label>
                <input type="text" required/>
                <br/>
                <label>Email</label>
                <input type="email" required/>
                <br/>
                <button>Save</button>
            </form>
            <br/>
            <label >Name <span style={{color: "red"}}>*</span></label>
            <input type="text" style={nameValiation ? {border: "1px solid red"} : {}} onChange={event => {
                setName(event.target.value)
                setNameValidation(false)
            }}/>
            <p style={{color: "red", fontSize: "12px"}}>{nameValiation ? "Name is required" : ""}</p>

            <label >Email <span style={{color: "red"}}>*</span></label>
            <input type="text" style={nameValiation ? {border: "1px solid red"} : {}} onChange={event => {
                setEmail(event.target.value);
                setEmailValidation(false);
            }}/>
            <p style={{
                color: "red",
                fontSize: "12px"
            }}>{emailValiation && email ? "Invalid Email" : emailValiation? "Email is required" : ""}</p>
            <button onClick={() => {
                customValidation()
            }}>next
            </button>
        </Layout>
    );
}