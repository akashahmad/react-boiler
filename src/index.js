import React from 'react';
import ReactDOM from 'react-dom';
import './assets/style/style.css'
import { Provier } from './store';

import Main from './App';
ReactDOM.render(<div>
    <Provier>
        <Main />
    </Provier>
</div>, document.getElementById('root'));
