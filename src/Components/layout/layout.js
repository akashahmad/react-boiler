import React from 'react';
import Header from '../header/header'
import Footer from '../footer/footer'
export default (props) => {
    return (
        <div>
            <h1>{props.word}</h1>
            <Header />
            {props.children}
            <Footer />
        </div>
    );
}