import React,{useEffect, useState} from 'react';
export default (props) => {
    let {global, petOwner} = props;
    let {firstName} = petOwner
    const [localState, setLocalState] = useState()
    useEffect(()=>{
        console.log("trigger");
        setLocalState(global+"Ok");
    },[global]);
    return (
        <>
            <h1>{firstName}</h1>
        </>
    );
}