import React, {useState} from 'react';
import Navigation from '../navigation/navigation'
export default () => {
    const [status, setStatus] = useState(false);
    const customSwitch = () => {
        setStatus(!status);
    };

    console.log("show State", status);
    return (
        <>
        <Navigation status={status} customSwitch={customSwitch}/>
        </>
    );
}
