import React from 'react';
import {Link} from 'react-router-dom';
export default (props) => {
    return (
        <ul style={{display: "flex", justifyContent: "space-around"}}>
            <li onClick={() => props.customSwitch()}><Link to="/">Home</Link></li>
            <li><Link to="/team">Team</Link></li>
            <li><Link to="/search">Search</Link></li>
            <li><Link to="/validation">Validatio</Link></li>
            {props.status && <li><Link to="/about">About</Link></li>}
        </ul>
    );
}